# Közérdekű közlemények

## Alapadatok

* Az itt található anyagok az Algoritmusok és Adatszerkezetek 2. című tárgy 2019/20. őszi félévének 11. és 12. csoportjának szólnak
* 11. csoport: szerdánként, 18.30 - 20.00, D 00-113
* 12. csoport: szerdánként, 16.30 - 18.00, D 00-112
* Mint látható, a két kurzus órái egymás után kerülnek megtartásra. Mivel eleve célom, hogy a két órán ugyanazt az anyagot adjam le, van lehetőség a másik csoport órájára járni (akár hetente váltakozva is, akár mindkét órára is), az alábbi két feltétellel:
    * Ha fizikailag nincs hely, annak van elsőbbsége, aki az adott órára jár papír szerint
    * Ha zh van az adott héten, előtte legalább 24 órával írj, s várd meg a visszajelzésem, mert olyankor nem akarom hagyni, hogy valakinek ne jusson kényelmesen hely
* Ha bármi miatt kérem/kérik, hogy jelezd, melyik órára jársz hivatalosan ill. a gyakorlatban, akkor kérlek ne "szerdai"-ként hivatkozz rá, mert mint láthatod, mindkettő az

## Elérhetőség

* Email: szitab@ik.elte.hu
* Körüzeneteket neptunból fogok küldeni - minden alkalommal megkaptok mindkét csoportra vonatkozó infókat
    * Legyen megadva éles email-cím, hogy egyből értesülj mindenről
* Beadandó-beadások, személyre szabott üzenetek, eredmények: https://canvas.elte.hu (belépés neptunnal)
    * Itt is legyen beállítva az emailes értesítés
* Közös információ, tananyag, beadandó-feladatkiírások: https://gitlab.com/elte-ik-algo/2019-2020-1-algo-2/
* Külön, automatikus konzultációs időpont nincs (mint ahogy szobám sincs), kérdés esetén emailben elérhető vagyok és ha online nem tudjuk megoldani, lehet kérni személyes konzultációt egyedi időpontra
* Órákon, zh-kon az algoritmusokat struktogram formájában adom meg és lehetőleg úgy is kérem vissza (vizsgán mindenképp). Online anyagokban pszeudokódként adom meg és online beadott szorgalmi feladat megoldásoknál el is fogadom a pszeudokódos megoldásokat

## Követelmények

### Előfeltétel

* Feltételezem, hogy az Algo 1-es kurzust elvégeztétek, és kb. képben vagytok az ottani tudásanyaggal
* Olyan témát, ami ott szerepelt, magamtól nem fogok újra elmondani, kérésre persze ismételhetünk
* Amire biztos szükség lesz:
    * Láncolt listák (láncolt ábrázolások, fejelemesség, többirányúság)
    * Rendezések
    * Fák (ábrázolások, bejárások)
    * Műveletigény (nem matematikai definíció szintjén, de a koncepciót érteni, ismerni, használni)
* Tavalyi gyakorlati jegyzetemet itt találjátok: https://gitlab.com/elte-ik-algo/2018-2019-2-algo-1/

### Idei tematika

* A gyakorlatokon előreláthatólag ebben a sorrendben és kb. ilyen arányokban vesszük az alábbi témákat. A témazárókat is ilyen felosztásban tervezem íratni
    * Veszteségmentes adattömörítés (kb. 1 órás anyag)
    * AVL-fák (2)
    * B+-fák és általános fák (1)
    * Gráfok. Alapok, BFS, Dijkstra-algoritmus és barátai (4)
    * Gráfok. DFS, MST, Floyd-Warshall és barátai (3)
    * Mintaillesztés (1)
* Az első téma - tömörítés - bár csak gyakorlaton fordul elő, előadáson nem, de éppúgy vizsgaanyag, mint az összes többi!

### Órák

* Jelenlét nem kötelező
* Mégis számon fogom tudni tartani a bejárási statisztikákat, s akik kitartóbban látogatják az órát, félév végén esetleges két jegy közötti állásnál, jobb elbírálásban lehet részük (az mindegy, hogy kettesért vagy négyesért küzd az illető)
* Az első órán nem állt módomban a jelenlétet ellenőrizni - itt mindenki megkapja a jelenlétét nyugtázó pontot
* Amennyiben valamelyik óra a csoportok tagjainak zömét érintő (évfolyamszintű) egyéb megpróbáltatással (analízis zh) ütközik, az adott órát elhalasztjuk. Ezt kérem, előre jelezzétek! Nemzeti ünnep miatt egy óra marad el, tehát összesen 12 db alkalommal számolok. Az esetleges pótórák idejéről szavazással döntünk
* Első órai többségi szavazás alapján végül abban állapodtunk meg, hogy minden olyan anyagot, amit úgy érzek, hogy a kurzus elsajátításához szükséges, alaposan leadok és ide fel is töltök. Ezzel azt kockáztatjuk, hogy nem érünk az anyag végére. Ha ez így történik, a bele nem férő anyagrészt zh-n nem fogom visszakérni, ám szorgalmi feladatokat írhatok ki hozzá, és mivel a vizsgán előfordulhatnak, segédanyagot is fogok feltölteni ezekhez
* Elképzelhető még több segédanyag felöltése is, ezeket érdemes átolvasni, felhasználni, de a kurzus elvileg nélkülük is teljesíthető. Új ismereteket nem fognak hordozni, csak a tanultakat ismételni, elmélyíteni

### Szorgalmi feladatok

* A félév során összesen 6 alkalommal (első óra és zh-k alkalmával nem) egy rövid "kvíz", amire az alábbi pontokat lehet kapni:
    * 0 pont - ha nincs beadva (~hiányzás)
    * 2 pont - ha be van adva és hibátlan
    * 2 pont - ha nincs a csoportban legalább 5 db hibátlan, s a kvíz az összes csoporttagot számolva a helyesség és gyorsaság szempontjából a legjobb 5 megoldás között van
    * 1 pont - különben
* Bizonyos órai kérdésekre a leggyorsabban jó választ adó (szóban vagy táblánál) kaphat 1 pontot
    * Ezt a lehetőséget a konkrét feladat ismertetése előtt bejelentem
    * Legalább 5 ilyen alkalom várható a félévben
* Órákon változó mennyiségben feladok egy-egy "papíros" szorgalmit 1-1 pontért
    * Legalább 15 db várható a félévben
    * Ezek pontos szövegezése itt, a "00_Feladatok" mappa alatt lesz majd olvasható, beadni pedig a kurzushoz tartozó canvas-felületen kell, akár mint üzenet, akár mint csatolt fájl
    * Elképzelhető, hogy 0 pontos értékelés esetén egy új, szűkebb határidőt tartva az általam megállapított hibákat, hiányosságokat javítva, pótolva a pont megszerezhető - erre vonatkozó jelzést lásd a canvasban az értékelésnél
    * A határidő a canvason megadottak szerint alakul, ami várhatóan a feltöltés + 2 hét
* A félév folyamán az első 3 és a második 3 témához is kódolós szorgalmi feladatok kerülnek kiírásra
    * Mindkét alkalommal 5-5 feladatból lehet szabadon választani egyet-egyet
    * A kiírás és a beadás hasonlóan alakul, mint az előző pontban, de a határidő valamivel nagyvonalúbb (a vizsgaidőszak eleje) lesz
    * A határidőre, jó minőségben beadott megoldás értéke 5 pont
    * A gyanúsan hasonló megoldások 0 pontot érnek (esetleges eltérés ettől utólag kialakulhat, ha egyértelműen kiderült, hogy ki a forrás és ki a másoló)
    * A gyengébbnek értékelt beadandókat várhatóan még lehet javítani a visszajelzéseim alapján
    * A pontozás során általános szoftvertechnológiai szempontok és az algoritmus helyessége (és a tanultaknak megfeleltsége) fog számítani (részletesebb követelmények a feladatkiírásnál)
    * Dokumentációt nem kell írni (amíg minden egyértelmű), a nyelv tetszőleges

### Zárthelyik

* A félév során 5 db témazáró zárthelyi lesz, mind 16 pontos
* Az óra helyszínén és idejében írjuk ezeket, hosszuk változó, kb. 15-20 percesek lesznek - előttük/utánuk az óra folytatódik további anyagrészekkel
* Várhatóan 2 feladat lesz ezeken, egy lejátszós fókuszú és egy algoritmusírós
* Szerepelhet tanult algoritmust megadó, azt elemző, azt lejátszó feladat vagy olyan kreatívabb feladat, amit bár órán egy az egyben nem vettünk, de a tanultak alapján megoldható
* Egy feladatnak több része is lehet (lejátszás, elméleti kérdés, bizonyítás, vagy legalábbis indoklás, struktogram...)
* Zh hetében nincs kvíz
* A zh jelenléti ívként is szolgál, automatikusan (a 16 ponton túl) jár egy pont a beadott zh-ért
* A zh-k idejét legalább egy héttel az előtt bejelentem, írásban is
* A zh-k előtti órát követően kb. 20 perces konzultációkat tartok, melyekre az adott zh-ra gyakorló feladatokkal készülök
* "Tételsort" és minta zh-t elérhetővé teszek, de a zh feladattípusok jellegéből adódóan a valós zh a mintától igencsak eltérhet
* A zh-k tematikájával és időzítésével a fentebb megadott féléves tematikát igyekszem követni, ettől lehet persze eltérés, amit jelezni fogok
* A zh-ra készülésnek mindenképp legyen része az előadás-jegyzet átolvasása is; bár direkt csak előadás-anyagot nem fogok kérdezni, de a tanuláshoz jól jöhet
* Minimális elérendő pontszám nincs, ha anélkül is összejön a szükséges összpontszám, még megírni se kell
* Az összes zh-t külön-külön egyszer lehet javítani (technikailag akár egy, akár több időpontra bontva ezeket). Rontás esetén az eredeti pontszámot veszem figyelembe
* Ha a szükséges pontszám így se jön össze, még egyszer meg lehet írni a zh-kat, de ekkor kötelezően az ötöt egyben 80 pontért (~gyakorlati utóvizsga) - rontani itt se lehet

### Egyéb

* A részvétel a 2019. 09. 19-ei 5vös5km futóversenyen (bemutatott rajtszámmal, vagy bármi más bizonyítékkal) 5 pontot ér
* A honlapon található első súlyos helyesírási, vagy akármilyen tartalmi hiba megtalálójának (emailben vagy itt Merge Requestként lehet jelezni) 1 pont jár
* A kisebb helyesírási, elgépelési, stilisztikai hibák esetében pedig minden 5. után jár 1-1 pont

### Disclaimer
* Az óráról órára tanulást elváró "témazárós" rendszert 24:3 arányban; az órai kvízeket 27:1 arányban; a lassabb, de biztosabb haladást 15:12 arányban Ti szavaztátok meg az első órán

### Jegyszerzés

* A fentieknek megfelelően a szorgalmi időszak utolsó napjáig előáll egy összpontszám
* Ezt pótzh-val és azok sikertelensége esetén gyakuv-val lehet majd még javítani (lásd fentebb)
* Ha csak egy-két pont hiányzik, a szorgalmasabbaknak fel fogok ajánlani olyan lehetőséget, hogy (lejárt, be nem adott) szorgalmik közül 4-5 db elkészítésével megadom a jobb jegyet
* A szorgalmi időszak végén írni fogok egy körlevelet, amiben jelzem, a végső pontszámok megszülettek. Arra kérek mindenkit, hogy akár elfogadja ezt a "megajánlott jegyet", akár nem, írjon vissza. Amennyiben elfogadja, a jegy bekerül a neptunba és csakis ez után az illető vizsgára jelentkezhet. Amennyiben nem fogadja el, írja meg, melyik zh-ból vagy zh-kból javítana, és mikor (lehetőleg több időpont-javaslatot is megjelölve). Hamarosan vissza fogok írni a pótzh idejéről és helyéről. A pótzh ha legalábbis a hallgató ezt igényli, a vizsgaidőszak első két hetében kerül megrendezésre. Ha később szeretné, ez nekem is megfelel, de akkor addig nem mehet el vizsgázni sem
* Gyakorlati jegy nélkül ne menjetek vizsgázni
* Ha a saját hibámból valakinek a szorgalmi időszak végére nincs kialakult gyakorlati jegye az alábbi kedvezményeket nyújtom "cserébe":
    * A beadható "kicsi" és "kódolós" szorgalmi feladatok határideje annulálódik, a vizsgaidőszak utolsó napjáig be lehet adni bármit, és bármeddig lehet javítani
    * El lehet menni előre vizsgázni, igazolom a nem beírt gyakorlati jegyet - itt persze, ha a félév legvégéig nem lesz meg a jegy, a vizsgajegy is törlődni fog!
* A jegyszerzésnél az alábbi ponthatárokat veszem figyelembe (félév során elért összes pont):
    * 80-csillagos ég (ez hibajavítások nélkül kb. 133-at jelent) - jeles
    * 70-79 - jó
    * 60-69 - közepes
    * 50-59 - elégséges
    * 0-49 - elégtelen
